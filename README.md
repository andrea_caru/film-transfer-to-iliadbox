## Introduction

This script allow you to transfer automatically any kind of file from a source (folder) to a destination (folder).
In this specific case, it is wused for transfer film from your Local PC to your Hard Disk Drive connected with your IliadBox (but it can be any network (or not) resources).

Some of features of this simple script are:
- Check the difference between the source folder and the destination folder (which file are not present in the destination folder) and make a copy only of these.
- Check if a file is under downloading, via checking the last modified date. If it is > 5 mins then the file is completed and ready to copy
- Progress bar during the copy

In this case, this script is used in a scheduled task on Windows 10, every day, for automatically copying Films to my HDD which is connected at my IliadBox (HDD is a network resources in this case).

## Prerequisites

Runtime
- Python 3.10

Python libraries:
- os
- filecmp
- tqdm
- datetime
- pytz

You can install with `pip` command.

If you want to use with network resources (as in this case), check if you have all the necessary permissions to access to destination. In this specific case I don't need any authentication because I'm in private/home network.

## How to use

Feel free to use entirely or a part of this code.
Probably, you need to change this two global variables inside the code:
- `PATH_SOURCE`
- `PATH_DESTINATION`

Which are the source and destination path.

## How to run

- `git clone https://gitlab.com/andrea_caru/film-transfer-to-iliadbox.git`
- `cd film-transfer-to-iliadbox`
- `python3 script.py`