import os
from tqdm import tqdm
from datetime import datetime
import pytz

PATH_SOURCE = "F:/Film/T0" # Change this to your source path
PATH_DESTINATION = '//iliadbox_Server/hdd andrea portable/Film' # Change this to your destination path
FILM_TO_UPLOAD = []

tz = pytz.timezone('Europe/Rome') # Rome Timezone
fmt = '%Y-%m-%d %H:%M:%S' # For example: 2022-02-28 22:27:15

# Check the difference between destination and source and append items which are not present in destination folder in a list
def folder_diff():   
    source = os.listdir(PATH_SOURCE) # folder containing your files
    destination = os.listdir(PATH_DESTINATION) # the other folder
    for dir in source:
        #print(dir)
        #print(len(PATH_SOURCE + "/" + dir))
        if os.path.isdir(PATH_SOURCE + "/" + dir): 
            myfolder = os.listdir(PATH_SOURCE + "/" + dir)
            #print(myfolder)
            for item1 in myfolder:
                trovato = False
                for item2 in destination:
                    if(item1==item2):
                        trovato = True
                if(trovato == False):
                    if (item1.endswith('mkv')):
                        FILM_TO_UPLOAD.append(PATH_SOURCE + "/" + dir + "/" + item1)
        else:
            for item2 in destination:
                if(dir==item2):
                    trovato = True
            if(trovato == False):
                if (dir.endswith('mkv')):
                    FILM_TO_UPLOAD.append(PATH_SOURCE + "/" + dir)
            
# Check if file is in downloading state or have finished checking the last modified date. If is > 5 (mins) means that file is downloaded and ready to copy to destination folder.
def get_date_diff(sourceFile):
    ts = os.path.getmtime(sourceFile)
    lastModifiedDate = datetime.fromtimestamp(ts,tz).strftime('%Y-%m-%d %H:%M:%S')
    lastModifiedDateNew = datetime.strptime(lastModifiedDate, fmt)
    currentDate = datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S')
    currentDateNew = datetime.strptime(currentDate, fmt)
    td_mins = int(round(abs((lastModifiedDateNew - currentDateNew).total_seconds()) / 60))
    return td_mins

# Main program
folder_diff()
print(FILM_TO_UPLOAD)
for film in FILM_TO_UPLOAD:
    if not FILM_TO_UPLOAD:
        print("Already up to date to the destination")
    else:
        dest = os.path.basename(os.path.normpath(film))
        destFile = (PATH_DESTINATION + "/" + dest)
        sourceSize = os.path.getsize(film)
        td_mins = get_date_diff(film)
        if (td_mins > 5):
            print("Copying " + film + " ...")
            with open(film, 'rb') as f:
                with open(destFile, 'ab') as n:
                    with tqdm(ncols=60, total=sourceSize, bar_format='{l_bar}{bar} | Remaining: {remaining}') as pbar:
                        buffer = bytearray()
                        while True:
                            buf = f.read(24576)
                            n.write(buf)
                            if len(buf) == 0:
                                break
                            buffer += buf
                            pbar.update(len(buf))
            # Once finished the copy, remove the file
            os.chmod(film, 0o777)
            try:
                os.remove(film)
            except:
                print("Can't remove the file!")
            